import React, { useContext, useState } from 'react';
import { Badge, Button, OverlayTrigger, Popover } from 'react-bootstrap';
import { ThreeDotsVertical } from 'react-bootstrap-icons';

import { UserContext } from '../contexts/UserContext';

function Users({ user }) {
  const { makeAdmin, removeAdmin } = useContext(UserContext);

  const [showPopover, setShowPopover] = useState(false);
  const [show, setShow] = useState(false);

  const handleShowEdit = () => {
    setShow(true);
    setShowPopover(false);
  };

  const renderPopOver = () => {
    setShowPopover(!showPopover);

    setTimeout(() => {
      setShowPopover(false);
    }, 1500);
  };

  const handleRemoveAdmin = (id) => {
    removeAdmin(id);
  };

  const handleMakeAdmin = (id) => {
    makeAdmin(id);
  };

  const renderActionMenu = (
    <Popover id="popover-basic">
      {/* <Popover.Header as="h3">Choose Actions</Popover.Header> */}
      <Popover.Body className="d-flex flex-column p-0">
        <Button
          className="edit-button"
          variant=""
          size="md"
          //   onClick={() => handleShowOrder(product._id)}
        >
          Orders
        </Button>

        <Button
          className="edit-button"
          variant=""
          size="md"
          //   onClick={handleShowEdit}
        >
          Edit
        </Button>

        <Button
          className="delete-button"
          variant=""
          size="md"
          //   onClick={() => handleDelete(product._id)}
        >
          Delete
        </Button>
      </Popover.Body>
    </Popover>
  );
  return (
    <>
      <td>
        <div className="position-relative">
          {user.lastName}, {user.firstName}
          {/* <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
            {user.orderedProduct.length > 0 ? user.orderedProduct.length : ''}
          </span> */}
        </div>
      </td>
      <td>{user.email}</td>
      <td>{user.mobileNumber}</td>

      <td className="text-center">
        {user.isAdmin ? (
          <Badge
            pill
            className="bg-success-subtle text-success-emphasis"
            style={{
              cursor: 'pointer'
            }}
            onClick={() => handleRemoveAdmin(user._id)}
          >
            Admin
          </Badge>
        ) : (
          <Badge
            pill
            className="bg-info-subtle text-success-emphasis"
            style={{
              cursor: 'pointer'
            }}
            onClick={() => handleMakeAdmin(user._id)}
          >
            Regular
          </Badge>
        )}
      </td>
      <td className="text-center">
        <OverlayTrigger
          placement="top"
          overlay={renderActionMenu}
          show={showPopover}
        >
          <ThreeDotsVertical
            style={{
              cursor: 'pointer'
            }}
            onClick={renderPopOver}
          />
        </OverlayTrigger>
      </td>
    </>
  );
}

export default Users;
