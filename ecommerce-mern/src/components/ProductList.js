import { useContext, useEffect, useState } from 'react';
import {
  Modal,
  Button,
  Alert,
  Table,
  Form,
  InputGroup,
  DropdownButton,
  Dropdown
} from 'react-bootstrap';

import {
  CaretUpFill,
  CaretDownFill,
  PlusCircleFill
} from 'react-bootstrap-icons';

import Pagination from './Pagination';
import { ProductContext } from '../contexts/ProductContext';

import AddForm from './AddForm';
import Product from './Product';

const ProductList = () => {
  let { products } = useContext(ProductContext);

  const [showAlert, setShowAlert] = useState(false);

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const [searchTerm, setSearchTerm] = useState('');

  const [isSortedPrice, setIsSortedPrice] = useState(false);
  const [isSortedName, setIsSortedName] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage] = useState(5);

  const [selectedFilter, setSelectedFilter] = useState('Name');

  const handleDropdownSelect = (selectedItem) => {
    setSelectedFilter(selectedItem);
  };

  const handleShowAlert = () => {
    setShowAlert(true);

    setTimeout(() => {
      setShowAlert(false);
    }, 1500);
  };

  const handleSortPrice = () => {
    setIsSortedPrice(!isSortedPrice);

    isSortedPrice
      ? products.sort((a, b) => (a.price > b.price ? -1 : 1))
      : products.sort((a, b) => (a.price < b.price ? -1 : 1));
  };

  const handleSortName = () => {
    setIsSortedName(!isSortedName);

    isSortedName
      ? products.sort((a, b) => (a.name > b.name ? -1 : 1))
      : products.sort((a, b) => (a.name < b.name ? -1 : 1));
  };

  useEffect(() => {
    handleClose();
  }, [products]);

  useEffect(() => {
    handleClose();

    return () => {
      handleShowAlert();
    };
  }, [products]);

  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProducts = products.slice(
    indexOfFirstProduct,
    indexOfLastProduct
  );

  const totalPagesNum = Math.ceil(products.length / productsPerPage);

  return (
    <>
      <div className="my-4 d-flex justify-content-between align-items-center">
        <div className="col-sm-6 p-0 w-25">
          <h3>
            Manage <b>Products</b> (
            {products.length ? products.length : 'Empty'})
          </h3>
        </div>
        <div className="col-sm-6 p-0 d-flex justify-content-end gap-2 w-50 align-items-center">
          <InputGroup className="w-75">
            <Form.Control
              placeholder={`Search by ${selectedFilter}`}
              onChange={(e) => {
                setSearchTerm(e.target.value);
                // handleSearch();
              }}
            />
            <DropdownButton
              variant="outline-secondary"
              title={selectedFilter}
              onSelect={handleDropdownSelect}
            >
              <Dropdown.Item eventKey="Name">Name</Dropdown.Item>
              <Dropdown.Item eventKey="Description">Description</Dropdown.Item>
              <Dropdown.Item eventKey="Price">Price</Dropdown.Item>
            </DropdownButton>
          </InputGroup>
          <Button
            onClick={handleShow}
            variant="success"
            size="md"
            data-toggle="modal"
            className="d-flex justify-content-between align-items-center gap-2 text-nowrap"
          >
            <PlusCircleFill />
            <span>Add Product</span>
          </Button>
        </div>
      </div>

      <Alert show={showAlert} variant="success">
        Updated Succefully!
      </Alert>

      <Table hover className="align-middle">
        <thead className="table-light">
          <tr>
            <th>Preview</th>
            <th className="d-flex gap-2">
              Name
              <div onClick={handleSortName}>
                {isSortedName ? <CaretUpFill /> : <CaretDownFill />}
              </div>
            </th>
            <th>Description</th>
            <th className="d-flex gap-2">
              Price
              <div onClick={handleSortPrice}>
                {isSortedPrice ? <CaretUpFill /> : <CaretDownFill />}
              </div>
            </th>
            <th className="text-center">Featured</th>
            <th className="text-center">Status</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {currentProducts
            .filter((product) => {
              if (searchTerm.toLowerCase() === '') {
                return product;
              } else if (selectedFilter === 'Name') {
                return product.name.toLowerCase().includes(searchTerm);
              } else if (selectedFilter === 'Description') {
                return product.description.toLowerCase().includes(searchTerm);
              } else if (selectedFilter === 'Price') {
                return product.price.toString().includes(searchTerm);
              }

              return product;
            })
            .map((product) => (
              <tr key={product._id}>
                <Product product={product} />
              </tr>
            ))}
        </tbody>
      </Table>

      <Pagination
        pages={totalPagesNum}
        setCurrentPage={setCurrentPage}
        currentProducts={currentProducts}
        numberOfProducts={products.length}
      />

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Products</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddForm />
        </Modal.Body>
        {/* <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close Button
          </Button>
        </Modal.Footer> */}
      </Modal>
    </>
  );
};

export default ProductList;
