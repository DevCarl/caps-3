import { useContext } from 'react';
import ActiveProduct from './ActiveProduct';
import { ProductContext } from '../contexts/ProductContext';

function ActiveProductList({ id }) {
  const { activeProducts } = useContext(ProductContext);

  const filteredProducts = activeProducts.filter(
    (product) => product.isFeatured !== true
  );

  return (
    <div className="row mt-3">
      {filteredProducts.map((product) => (
        <div className="col-lg-3 col-md-3" key={product._id}>
          <ActiveProduct product={product} />
        </div>
      ))}
    </div>
  );
}

export default ActiveProductList;
