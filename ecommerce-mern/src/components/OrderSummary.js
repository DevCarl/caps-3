import { Badge, Image } from 'react-bootstrap';
import { NumericFormat } from 'react-number-format';

function OrderSummary({ product }) {
  return (
    <div className="d-flex justify-content-between align-items-center">
      <div className="d-flex gap-3 justify-content-between align-items-center">
        <div
          style={{
            width: '60px',
            height: '60px',
            position: 'relative'
          }}
        >
          <Image
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              objectFit: 'cover'
            }}
            variant="top"
            src={product.image}
            alt={product.description}
          />
        </div>
        <strong>{product.productName}</strong>
      </div>

      <div className="d-flex gap-3 justify-content-between align-items-center">
        <span>
          <NumericFormat
            value={product.price.toFixed(2)}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'$'}
          />
        </span>
        <span>x</span>
        <span>{product.quantity}</span>
        <Badge pill className="bg-success-subtle text-success-emphasis">
          <NumericFormat
            value={product.subTotal.toFixed(2)}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'$'}
          />
        </Badge>
      </div>
    </div>
  );
}

export default OrderSummary;
