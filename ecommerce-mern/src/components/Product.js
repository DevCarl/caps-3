import { useContext, useEffect, useState } from 'react';

import { ThreeDotsVertical, StarFill } from 'react-bootstrap-icons';

import {
  Accordion,
  Badge,
  Button,
  Card,
  Image,
  Modal,
  OverlayTrigger,
  Popover,
  Tooltip
} from 'react-bootstrap';
import EditForm from './EditForm';

import { ProductContext } from '../contexts/ProductContext';
import ProductOrderItem from './ProductOrderItem';

const Product = ({ product }) => {
  const {
    archiveProduct,
    activateProduct,
    deleteProduct,
    featureProduct,
    unfeatureProduct
  } = useContext(ProductContext);

  const [showPopover, setShowPopover] = useState(false);

  const [show, setShow] = useState(false);
  const [showOrder, setShowOrder] = useState(false);

  const handleShowEdit = () => {
    setShow(true);
    setShowPopover(false);
  };
  const handleCloseEdit = () => setShow(false);

  useEffect(() => {
    handleCloseEdit();
  }, [product]);

  const [showImage, setShowImage] = useState(false);
  const handleImageClick = () => setShowImage(true);
  const handleCloseModal = () => setShowImage(false);

  const handleShowOrder = (id) => {
    setShowOrder(true);
  };

  const handleCloseOrder = (id) => {
    setShowOrder(false);
  };

  const handleArchive = (id) => {
    archiveProduct(id);
  };

  const handleActivate = (id) => {
    activateProduct(id);
  };

  const handleDelete = (id) => {
    deleteProduct(id);
  };

  const handleFeatured = (id) => {
    featureProduct(id);
  };

  const handleNotFeatured = (id) => {
    unfeatureProduct(id);
  };

  const renderImageTooltip = (text) => (
    <Tooltip id="image-tooltip">{text}</Tooltip>
  );

  const renderStatusTooltip = (text) => (
    <Tooltip id="image-tooltip">{text}</Tooltip>
  );

  const renderPopOver = () => {
    setShowPopover(!showPopover);

    setTimeout(() => {
      setShowPopover(false);
    }, 1500);
  };

  const renderActionMenu = (
    <Popover id="popover-basic">
      {/* <Popover.Header as="h3">Choose Actions</Popover.Header> */}
      <Popover.Body className="d-flex flex-column p-0">
        <Button
          className="edit-button"
          variant=""
          size="md"
          onClick={() => handleShowOrder(product._id)}
        >
          Orders
        </Button>

        <Button
          className="edit-button"
          variant=""
          size="md"
          onClick={handleShowEdit}
        >
          Edit
        </Button>

        <Button
          className="delete-button"
          variant=""
          size="md"
          onClick={() => handleDelete(product._id)}
        >
          Delete
        </Button>
      </Popover.Body>
    </Popover>
  );

  return (
    <>
      <td>
        <div
          style={{ width: '50px', height: '50px' }}
          className="position-relative"
        >
          <OverlayTrigger
            placement="top"
            overlay={renderImageTooltip('View Image')}
          >
            <Image
              src={product.image}
              rounded
              fluid
              onClick={handleImageClick}
              style={{
                width: '100%',
                height: '100%',
                objectFit: 'cover',
                cursor: 'pointer'
              }}
            />
          </OverlayTrigger>
          <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
            {product.userOrders.length > 0 ? product.userOrders.length : ''}
          </span>
        </div>
      </td>
      <td>
        {product.isFeatured ? (
          <StarFill className="me-2" style={{ color: '#FFABAB' }} />
        ) : (
          ''
        )}
        {product.name}
      </td>
      <td>{product.description}</td>
      <td>${product.price}</td>
      <td className="text-center">
        <OverlayTrigger
          placement="top"
          overlay={renderStatusTooltip('Feature Product')}
        >
          {product.isFeatured ? (
            <Badge
              pill
              className="bg-warning-subtle text-success-emphasis"
              style={{
                cursor: 'pointer'
              }}
              onClick={() => handleNotFeatured(product._id)}
            >
              Featured
            </Badge>
          ) : (
            <Badge
              pill
              className="bg-info-subtle text-success-emphasis"
              style={{
                cursor: 'pointer'
              }}
              onClick={() => handleFeatured(product._id)}
            >
              Regular
            </Badge>
          )}
        </OverlayTrigger>
      </td>
      <td className="text-center">
        <OverlayTrigger
          placement="top"
          overlay={renderStatusTooltip('Change Status')}
        >
          {product.isActive ? (
            <Badge
              pill
              className="bg-success-subtle text-success-emphasis"
              style={{
                cursor: 'pointer'
              }}
              onClick={() => handleArchive(product._id)}
            >
              Available
            </Badge>
          ) : (
            <Badge
              pill
              className="bg-danger-subtle text-success-emphasis"
              style={{
                cursor: 'pointer'
              }}
              onClick={() => handleActivate(product._id)}
            >
              Not Available
            </Badge>
          )}
        </OverlayTrigger>
      </td>
      <td className="text-center">
        <OverlayTrigger
          placement="top"
          overlay={renderActionMenu}
          show={showPopover}
        >
          <ThreeDotsVertical
            style={{
              cursor: 'pointer'
            }}
            onClick={renderPopOver}
          />
        </OverlayTrigger>
      </td>

      <Modal show={show} onHide={handleCloseEdit}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <EditForm theProduct={product} />
        </Modal.Body>
      </Modal>

      <Modal show={showOrder} onHide={handleCloseOrder}>
        <Modal.Header closeButton>
          <Modal.Title>Orders ({product.userOrders.length})</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Accordion className="">
            <Accordion.Item>
              <Accordion.Header>{product.name}</Accordion.Header>
              <Accordion.Body>
                <ProductOrderItem theProduct={product} />
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Modal.Body>
      </Modal>

      {/* Show image Preview */}
      <Modal show={showImage} centered onHide={handleCloseModal}>
        <Modal.Header>
          <Modal.Title>{product.name}</Modal.Title>
          <Badge
            pill
            className={
              'bg-' +
              (product.isActive ? 'success' : 'danger') +
              '-subtle text-success-emphasis'
            }
            style={{
              cursor: 'pointer'
            }}
            onClick={() => handleActivate(product._id)}
          >
            {product.isActive ? 'Available' : 'Not Available'}
          </Badge>
        </Modal.Header>
        <Modal.Body>
          <div style={{ width: 'auto', height: '400px' }}>
            <Image
              src={product.image}
              fluid
              style={{ width: '100%', height: '100%', objectFit: 'cover' }}
            />
          </div>

          <Accordion className="mt-3">
            <Accordion.Item>
              <Accordion.Header>
                ({product.userOrders.length}) Orders
              </Accordion.Header>
              <Accordion.Body>
                <ProductOrderItem theProduct={product} />
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Product;
