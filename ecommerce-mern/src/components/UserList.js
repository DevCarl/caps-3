import { useContext } from 'react';

import { UserContext } from '../contexts/UserContext';

import Users from './Users';
import { Table } from 'react-bootstrap';

const UserList = () => {
  let { allUsers, user } = useContext(UserContext);

  console.log(user.id);

  console.log(allUsers);

  return (
    <>
      <div className="my-4 d-flex justify-content-between align-items-center">
        <div className="col-sm-6 p-0 w-25">
          <h3>
            Manage <b>Users</b> ({allUsers.length ? allUsers.length : 'Empty'})
          </h3>
        </div>
      </div>

      <Table hover className="align-middle">
        <thead className="table-light">
          <tr>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile Number</th>

            <th className="text-center">Is Admin?</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          {allUsers
            .filter((filterUser) => filterUser._id !== user.id)
            .map((user) => (
              <tr key={user._id}>
                <Users user={user} />
              </tr>
            ))}
        </tbody>
      </Table>
    </>
  );
};

export default UserList;
