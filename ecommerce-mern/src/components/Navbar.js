import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

import logo from '../Links/YIKE.svg';

import { UserContext } from '../contexts/UserContext';
import { CartContext } from '../contexts/CartContext';
import { useContext, useEffect, useState } from 'react';
import { NavLink, Link } from 'react-router-dom';
import { Bag } from 'react-bootstrap-icons';
import { Image } from 'react-bootstrap';

function NavScrollExample() {
  const { user } = useContext(UserContext);
  const { cart } = useContext(CartContext);

  const [cartItems, setCartItems] = useState(0);

  useEffect(() => {
    setCartItems(cart.length);
  }, [cart]);

  console.log(cart);

  return (
    <Navbar
      collapseOnSelect
      expand="lg"
      sticky="top"
      className="bg-body-tertiary px-4"
    >
      <Container fluid={user.isAdmin ? false : true}>
        <Navbar.Brand className="" as={NavLink} to="/">
          <Image src={logo} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse
          className=" d-flex justify-content-end"
          id="responsive-navbar-nav"
        >
          <Nav className="d-flex gap-4">
            {user.isAdmin ? (
              <Nav.Link as={Link} to="/dashboard/orders">
                Orders
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={Link} to="/products">
                  Collection
                </Nav.Link>
                <Nav.Link as={Link} to="/featured">
                  Featured
                </Nav.Link>
              </>
            )}

            {user.isAdmin ? (
              <>
                <NavDropdown title="Dashboard" id="basic-nav-dropdown">
                  <NavDropdown.Item as={Link} to="dashboard/products">
                    Manage Products
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="dashboard/users">
                    Manage Users
                  </NavDropdown.Item>
                </NavDropdown>
              </>
            ) : (
              ''
            )}

            {user.id === null || user.id === undefined ? (
              <div className="d-flex gap-2">
                <Button
                  variant="outline-dark"
                  className="rounded-pill"
                  as={Link}
                  to="/login"
                >
                  Login
                </Button>
                <Button
                  variant="dark"
                  as={Link}
                  className="rounded-pill"
                  to="/register"
                >
                  Sign up
                </Button>
              </div>
            ) : (
              <div className="d-flex">
                <NavDropdown title={user.firstName} id="basic-nav-dropdown">
                  {/* <NavDropdown.Item href="#action3">Register</NavDropdown.Item> */}
                  {user.isAdmin ? (
                    ''
                  ) : (
                    <NavDropdown.Item as={Link} to="/orders">
                      My Orders
                    </NavDropdown.Item>
                  )}
                  <NavDropdown.Item as={Link} to="/logout">
                    Logout
                  </NavDropdown.Item>
                </NavDropdown>
              </div>
            )}

            {user.id === null || user.id === undefined ? (
              ''
            ) : user.isAdmin ? (
              ''
            ) : (
              <Nav.Link as={Link} to="/cart">
                <Bag /> ({cartItems})
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavScrollExample;
