import { useContext } from 'react';

import { ProductContext } from '../contexts/ProductContext';
import FeaturedProduct from './FeaturedProduct';

function FeaturedProductList({ id }) {
  const { activeProducts } = useContext(ProductContext);

  const filteredProducts = activeProducts.filter(
    (product) => product.isFeatured !== false
  );

  return (
    <div className="row mt-3">
      {filteredProducts.map((product) => (
        <div className="col-lg-4 col-md-4" key={product._id}>
          <FeaturedProduct product={product} />
        </div>
      ))}
    </div>
  );
}

export default FeaturedProductList;
