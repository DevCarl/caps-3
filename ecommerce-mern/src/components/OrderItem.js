import { Badge, Image } from 'react-bootstrap';
import { NumericFormat } from 'react-number-format';

function OrderItem({ orderDetails }) {
  const products = orderDetails.products;

  console.log(orderDetails);

  return (
    <div className="card border-light">
      <div class="card-header p-3">
        <span className="text-muted">
          {new Date(orderDetails.purchasedOn).toLocaleString()}
        </span>
      </div>
      <div class="card-body p-3 d-flex flex-column gap-2">
        <div className="d-flex flex-column gap-3">
          {products.map((product) => (
            <div className="d-flex gap-4 align-items-center justify-content-between">
              <div className="d-flex gap-4 align-items-center">
                <div
                  style={{
                    width: '100px',
                    height: '100px',
                    position: 'relative'
                  }}
                >
                  <Image
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      objectFit: 'cover'
                    }}
                    variant="top"
                    src={product.image}
                    alt={product.description}
                  />
                </div>

                <div className="d-flex flex-column">
                  <span>{product.productName}</span>
                  <span className="text-muted">{product.description}</span>
                </div>
              </div>

              <div className="d-flex gap-4">
                <NumericFormat
                  value={product.subTotal.toFixed(2)}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'$'}
                />
                <div className="d-flex gap-4">
                  <Badge
                    pill
                    className="bg-success-subtle text-success-emphasis d-flex justify-content-between align-items-center "
                  >
                    Quantity: {product.quantity}
                  </Badge>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div class="card-footer p-3">
        <div className="d-flex justify-content-between ">
          <strong>Total</strong>
          <strong>
            <NumericFormat
              value={orderDetails.totalAmount.toFixed(2)}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'$'}
            />
          </strong>
        </div>
      </div>
    </div>
  );
}

export default OrderItem;
