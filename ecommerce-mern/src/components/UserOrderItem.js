import React from 'react';
import { Badge, Image } from 'react-bootstrap';
import { NumericFormat } from 'react-number-format';

function UserOrderItem({ order }) {
  console.log(order);
  return (
    <>
      {order.orderedProduct.map((product) => (
        <div className="card border-light pb-4" key={product.id}>
          <div className="card-header p-3">
            <span className="text-muted">
              {new Date(product.purchasedOn).toLocaleString()}
            </span>
          </div>
          <div className="card-body p-3 d-flex flex-column gap-2">
            <div className="d-flex flex-column gap-3">
              {product.products.map((innerProduct) => (
                <div
                  key={innerProduct.id}
                  className="d-flex gap-4 align-items-center justify-content-between"
                >
                  <div className="d-flex gap-4 align-items-center">
                    <div
                      style={{
                        width: '100px',
                        height: '100px',
                        position: 'relative'
                      }}
                    >
                      <Image
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          width: '100%',
                          height: '100%',
                          objectFit: 'cover'
                        }}
                        variant="top"
                        src={innerProduct.image}
                        alt={innerProduct.description}
                      />
                    </div>

                    <div className="d-flex flex-column">
                      <span>{innerProduct.productName}</span>
                      <span className="text-muted">
                        {innerProduct.description}
                      </span>
                    </div>
                  </div>

                  <div className="d-flex gap-4">
                    <NumericFormat
                      value={innerProduct.subTotal.toFixed(2)}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'$'}
                    />
                    <div className="d-flex gap-4">
                      <Badge
                        pill
                        className="bg-success-subtle text-success-emphasis d-flex justify-content-between align-items-center "
                      >
                        Quantity: {innerProduct.quantity}
                      </Badge>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div class="card-footer p-3">
            <div className="d-flex justify-content-between ">
              <strong>Total</strong>
              <strong>
                <NumericFormat
                  value={product.totalAmount.toFixed(2)}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'$'}
                />
              </strong>
            </div>
          </div>
        </div>
      ))}
    </>
  );
}

export default UserOrderItem;
