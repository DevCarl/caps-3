import AdminDashboard from './pages/AdminDashboard';
import Navbar from './components/Navbar';
import Login from './pages/Login';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import ProductContextProvider from './contexts/ProductContext';
import UserContextProvider from './contexts/UserContext';
import CartContextProvider from './contexts/CartContext';
import OrderContextProvider from './contexts/OrderContext';

import Footer from './components/Footer';
import Logout from './pages/Logout';
import Cart from './pages/Cart';
import Home from './pages/Home';
import Register from './pages/Register';
import ActiveProductView from './components/ActiveProductView';
import AllProducts from './pages/AllProducts';
import Checkout from './pages/Checkout';
import { Container } from 'react-bootstrap';
import Orders from './pages/Orders';
import PageNotFound from './pages/PageNotFound';
import UserOrders from './pages/UserOrders';
import ManageUsers from './pages/ManageUsers';
import FeaturedProductList from './components/FeaturedProductList';

function App() {
  return (
    <>
      <BrowserRouter>
        <UserContextProvider>
          <ProductContextProvider>
            <CartContextProvider>
              <OrderContextProvider>
                <Navbar />
                <Container fluid className="px-4 pt-4">
                  <Routes>
                    <Route path="/" element={<Home />} />
                    <Route
                      path="/dashboard/products"
                      element={<AdminDashboard />}
                    />
                    <Route path="/dashboard/users" element={<ManageUsers />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route
                      path="/products/:id"
                      element={<ActiveProductView />}
                    />
                    <Route path="/products" element={<AllProducts />} />
                    <Route path="/featured" element={<FeaturedProductList />} />
                    <Route path="/orders" element={<Orders />} />
                    <Route path="/dashboard/orders" element={<UserOrders />} />
                    <Route path="/cart" element={<Cart />} />
                    <Route path="/checkout" element={<Checkout />} />
                    <Route path="*" element={<PageNotFound />} />
                  </Routes>
                </Container>
                {/* <Footer /> */}
              </OrderContextProvider>
            </CartContextProvider>
          </ProductContextProvider>
        </UserContextProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
