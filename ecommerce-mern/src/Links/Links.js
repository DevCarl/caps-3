export const images = {
  hero: 'https://mir-s3-cdn-cf.behance.net/project_modules/fs/70cfeb105946685.5f84c94fc9da7.jpg',
  hero1:
    'https://loqueva.com/wp-content/uploads/2022/08/LE-COQ-SPORTIF-la-reconocida-marca-francesa-lanza-en-la-Argentina-su-nueva-coleccion-de-zapatillas-2.jpg',
  hero2:
    'https://mir-s3-cdn-cf.behance.net/project_modules/1400/52ec7c153930421.6338b8b9b1284.jpg',
  hero3:
    'https://mir-s3-cdn-cf.behance.net/project_modules/1400/718b3f153930421.6338b8b9afca0.jpg',
  hero4:
    'https://images.augustman.com/wp-content/uploads/2021/05/06151823/Dolce_Gabbana_DayMaster_Main.jpg?tr=w-1920',
  hero5:
    'https://graziamagazine.com/us/wp-content/uploads/sites/15/2023/05/jacquemus-nike-2023-collab.jpg',

  hero6:
    'https://static.nike.com/a/images/f_auto/dpr_1.0,cs_srgb/w_1452,c_limit/bec52a93-3f23-4f71-b5e7-c4f180e35a73/welcome-to-jordan-basketball.jpg',

  hero7:
    'https://static.nike.com/a/images/f_auto/dpr_1.0,cs_srgb/w_1824,c_limit/01c9f9b0-fb38-404b-91cb-443e7874b09a/welcome-to-jordan-basketball.jpg',
  hero8:
    'https://mir-s3-cdn-cf.behance.net/project_modules/1400/052a6c107900021.5fb22745b63f0.jpg',

  login:
    'https://mir-s3-cdn-cf.behance.net/project_modules/fs/b95595144948231.6295ce4b0b704.jpg',

  heroVideo: 'https://streamable.com/le6wrn'
};
