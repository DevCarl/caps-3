import React, { useContext } from 'react';
import { Accordion, Badge, Image } from 'react-bootstrap';
import { NumericFormat } from 'react-number-format';
import { OrderContext } from '../contexts/OrderContext';
import UserOrderItem from '../components/UserOrderItem';

function UserOrders() {
  const { userOrder } = useContext(OrderContext);
  console.log(userOrder);

  return (
    <div>
      <div className="container">
        <div className="row d-flex justify-content-center align-items-center">
          <div className="col-lg-12 ">
            <div className=" ">
              <h3 className="mb-4">Order History</h3>
              <div className="d-flex flex-column gap-4">
                <Accordion>
                  {userOrder.reverse().map((order, index) => (
                    <Accordion.Item eventKey={index}>
                      <Accordion.Header>
                        Orders for user
                        <Badge
                          pill
                          className="bg-success-subtle text-success-emphasis d-flex justify-content-between align-items-center ms-3 p-2 px-3"
                        >
                          {order.email}
                        </Badge>
                      </Accordion.Header>
                      <Accordion.Body>
                        <UserOrderItem order={order} />
                      </Accordion.Body>
                    </Accordion.Item>
                  ))}
                </Accordion>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserOrders;
