import { useContext } from 'react';
import ProductList from '../components/ProductList';
import { UserContext } from '../contexts/UserContext';
import { useNavigate } from 'react-router-dom';

function App() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  console.log(user);
  return user.isAdmin ? (
    <div className="container-xl">
      <div className="table-responsive">
        <div className="table-wrapper">
          <ProductList />
        </div>
      </div>
    </div>
  ) : (
    navigate('/')
  );
}

export default App;
