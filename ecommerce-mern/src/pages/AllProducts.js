import { useContext, useEffect, useRef, useState } from 'react';
import { Button, Form, Image } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import ActiveProductList from '../components/ActiveProductList';

import { CartContext } from '../contexts/CartContext';

function AllProducts() {
  const { id } = useParams();

  const { addToCart } = useContext(CartContext);

  const [name, setName] = useState('');
  const [initialPrice, setInitialPrice] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');

  const [quantity, setQuantity] = useState(1);

  const productRef = useRef(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setInitialPrice(data.price);
        setPrice(data.price);
        setDescription(data.description);
        setImage(data.image);

        // Set focus on the product element after data is loaded
        if (productRef.current) {
          productRef.current.focus();
        }
      });

    setQuantity(1);
  }, [id]);

  useEffect(() => {
    const calculatedPrice = initialPrice * quantity;
    setPrice(calculatedPrice.toFixed(2));
  }, [quantity]);

  const handleAddToCart = () => {
    addToCart(id, quantity);
  };

  const quantityOptions = Array.from({ length: 10 }, (_, index) => (
    <option key={index + 1} value={index + 1}>
      {index + 1}
    </option>
  ));

  return (
    <div>
      <ActiveProductList id={id} />
    </div>
  );
}

export default AllProducts;
