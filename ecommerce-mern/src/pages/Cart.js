/* eslint-disable jsx-a11y/anchor-is-valid */
import { useContext } from 'react';
import { Badge, Button } from 'react-bootstrap';

import { CartContext } from '../contexts/CartContext';

import { Link } from 'react-router-dom';

import CartItem from '../components/CartItem';
import { NumericFormat } from 'react-number-format';

function Cart() {
  const { cart, total } = useContext(CartContext);

  return (
    <div>
      <div className="container">
        <div className="row mt-3" id="product">
          <div className="col-lg-6 ">
            <div className="p-4 d-flex flex-column gap-1">
              <h4 className="mb-4">Your Cart ({cart.length}) </h4>
              {cart.map((product) => (
                <div key={product._id}>
                  <CartItem cart={product} />
                </div>
              ))}
            </div>
          </div>
          <div className="col-lg-5 ">
            <div className=" p-4 d-flex flex-column gap-1">
              <h4 className="mb-4">Order Summary</h4>

              <div className="d-flex justify-content-between">
                <span>Subtotal</span>
                <NumericFormat
                  value={total.toFixed(2)}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'$'}
                />
              </div>

              <div className="d-flex justify-content-between">
                <span>Estimated Delivery & Handling</span>

                <Badge
                  pill
                  className="bg-success-subtle text-success-emphasis d-flex justify-content-between align-items-center"
                >
                  Free
                </Badge>
              </div>

              <hr className="divider" />
              <div className="d-flex justify-content-between">
                <strong>Total</strong>
                <strong>
                  <NumericFormat
                    value={total.toFixed(2)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'$'}
                  />
                </strong>
              </div>

              <Button
                variant="dark"
                className="p-3 mt-3 rounded-pill"
                // onClick={handleCheckOut}
                as={Link}
                to="/checkout"
              >
                Proceed Checkout
              </Button>
              <Button
                variant="outline-dark"
                className="p-3 mt-2 rounded-pill"
                as={Link}
                to="/products"

                // onClick={() => handleAddToCart(id)}
              >
                Shop More
              </Button>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="d-flex justify-content-between align-items-end mt-5">
        <h4>You might also like</h4>
        <span>See All</span>
      </div>
      <ActiveProductList id={id} /> */}
    </div>
  );
}

export default Cart;
