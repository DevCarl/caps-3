import React, { useContext } from 'react';

import { images } from '../Links/Links.js';

import { UserContext } from '../contexts/UserContext.js';

function PageNotFound() {
  const { authenticate } = useContext(UserContext);

  const imageUrl = images.login;

  return (
    <div
      className="bg-success fullscreen d-flex justify-content-center align-items-center w-100 m-auto "
      style={{
        backgroundImage: `url(${imageUrl})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'top'
      }}
    >
      <div className="w-50 bg-light p-4 rounded text-center">
        <h1>
          We can't find the page you are looking for. Sorry for the
          inconvenience.
        </h1>
      </div>
    </div>
  );
}

export default PageNotFound;
