import { createContext, useEffect, useState } from 'react';

export const ProductContext = createContext();

const ProductContextProvider = (props) => {
  const [products, setProducts] = useState([]);
  const [activeProducts, setActiveProducts] = useState([]);

  const [allFeaturedProducts, setAllFeaturedProducts] = useState([]);

  const sortProducts = (products, sortCriteria) => {
    if (sortCriteria === 'id') {
      return products.sort((a, b) => (a._id > b._id ? -1 : 1));
    } else if (sortCriteria === 'name') {
      return products.sort((a, b) => a.name.localeCompare(b.name));
    } else if (sortCriteria === 'price') {
      return products.sort((a, b) => a.price - b.price);
    }

    return products;
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/featured`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setAllFeaturedProducts(data.products);
        setProducts(sortProducts(data.products, 'id'));
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setActiveProducts(sortProducts(data.products, 'id'));
      });
  }, [activeProducts]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setProducts(sortProducts(data.products, 'id'));
      });
  }, []);

  const addProduct = (image, name, description, price) => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify([
        {
          image,
          name,
          description,
          price
        }
      ])
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  const updateProduct = (id, image, name, description, price) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        image,
        name,
        description,
        price
      })
    })
      .then((response) => response.json())
      .then((data) => {
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  const archiveProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  const featureProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/feature`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        const featuredProducts = products.filter(
          (product) => product.isFeatured === true
        );
        setAllFeaturedProducts(featuredProducts);
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  const unfeatureProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/unfeature`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data.products);
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  const activateProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/activate`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  const deleteProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/delete`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setProducts(sortProducts(data.products, 'id'));
      });
  };

  return (
    <ProductContext.Provider
      value={{
        products,
        activeProducts,
        allFeaturedProducts,
        featureProduct,
        unfeatureProduct,
        addProduct,
        updateProduct,
        archiveProduct,
        activateProduct,
        deleteProduct,
        sortProducts
      }}
    >
      {props.children}
    </ProductContext.Provider>
  );
};

export default ProductContextProvider;
