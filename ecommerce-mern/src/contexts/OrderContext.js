import { createContext, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

import { CartContext } from './CartContext';
import { UserContext } from './UserContext';

export const OrderContext = createContext();

const OrderContextProvider = (props) => {
  const navigate = useNavigate();

  const { setCart } = useContext(CartContext);
  const { user } = useContext(UserContext);

  const [order, setOrder] = useState([]);
  const [myOrder, setMyOrder] = useState([]);

  const [userOrder, setUserOrder] = useState([]);

  const checkout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setCart([]);
        setOrder(data.OrderedProduct);
        retrieveOrderDetails();
        Swal2.fire({
          title: 'Payment Successful',
          icon: 'success'
          //   text: 'Welcome to Shoppepe!'
        });
        navigate('/');
      });
  };

  const retrieveOrderDetails = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setMyOrder(data.orderedProduct);
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setUserOrder(data.orders);

        // data.orderedProduct.map((product) => console.log(product));
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setMyOrder(data.orderedProduct);

        // data.orderedProduct.map((product) => console.log(product));
      });
  }, [user]);

  return (
    <OrderContext.Provider value={{ checkout, order, myOrder, userOrder }}>
      {props.children}
    </OrderContext.Provider>
  );
};

export default OrderContextProvider;
