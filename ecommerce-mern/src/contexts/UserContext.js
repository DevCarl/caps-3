import { createContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export const UserContext = createContext();

const UserContextProvider = (props) => {
  const navigate = useNavigate();
  const [user, setUser] = useState({
    id: null,
    firstName: null,
    isAdmin: null,
    addedToCart: null
  });

  const [allUsers, setAllUsers] = useState([]);

  const unsetUser = () => {
    localStorage.clear();
  };

  const register = (firstName, lastName, mobileNumber, email, password) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName,
        lastName,
        mobileNumber,
        email,
        password
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 'error') {
          Swal2.fire({
            title: 'Register unsuccessful!',
            icon: 'error',
            text: data.message
          });
        } else {
          navigate('/login');
          Swal2.fire({
            title: 'Registered successfully!',
            icon: 'success',
            text: 'Welcome to Shoppepe!'
          });
        }
      });
  };

  const authenticate = (email, password) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 'error') {
          Swal2.fire({
            title: 'Login unsuccessful!',
            icon: 'error',
            text: data.message
          });
        } else {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);
          Swal2.fire({
            title: 'Login successful!',
            icon: 'success',
            text: 'Welcome to Shoppepe!'
          });
        }
      });
  };

  const retrieveUserDetails = (token) => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          firstName: data.firstName,
          isAdmin: data.isAdmin,
          addedToCart: data.addedToCart
        });

        data.isAdmin ? navigate('/dashboard/products') : navigate('/');
      });
  };

  const makeAdmin = (id) => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data.users);
        setAllUsers(data.users);
      });
  };

  const removeAdmin = (id) => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/${id}/removeAdmin`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data.users);
        setAllUsers(data.users);
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({
          id: data._id,
          firstName: data.firstName,
          isAdmin: data.isAdmin,
          addedToCart: data.addedToCart
        });
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setAllUsers(data);
      });
  }, []);

  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        unsetUser,
        authenticate,
        register,
        allUsers,
        makeAdmin,
        removeAdmin
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};

export default UserContextProvider;
